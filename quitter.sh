#!/bin/bash

################################################################################
#
#       Gère des événements
#
#       Créé par Marius Pardo et Ivan Canet
#
#       "LICENCE BEERWARE" (Révision 42):
#       <marius.pardo@etu.u-bordeaux.fr> et <ivan.canet@etu-bordeaux.fr> ont
#       créé ce fichier. Tant que vous conservez cet avertissement, vous pouvez
#       faire ce que vous voulez de ce truc. Si on se rencontre un jour et que
#       vous pensez que ce truc vaut le coup, vous pouvez nous payer une bière en
#       retour.
#
################################################################################

# Fichiers de configuration
DOSSIER_CONFIGURATION="$HOME/.config/quitter"
#DOSSIER_CONFIGURATION="/tmp/quitter" #provisoire pendant les tests
FICHIER_BOUCLE="$DOSSIER_CONFIGURATION/boucle.pid"
FICHIER_HORAIRE="$DOSSIER_CONFIGURATION/horaire.db"
FICHIER_TMP="/tmp/quitter_tmp" # Fichier dans /tmp, donc pas de suppression nécessaire ; utilisé par la boucle

# Constantes
let BOUCLE_TEMPS_MISE_A_JOUR=30 # Temps entre deux mises à jour de la boucle (secondes)
let DEBUG=1 # 0 Débug activé, 1 débug désactivé

function lancerCommeBoucle {
    [[ -a $FICHIER_TMP ]] || touch $FICHIER_TMP
    while true # Boucle infinie
    do
        debug "Vérification de l'heure :"
        let heure=10#$(date +%H%M)
        debug $heure
        cp $FICHIER_HORAIRE $FICHIER_TMP # Pour éviter de modifier le fichier qu'on est en train de lire
        # Lecture de la base de données
        # Cette boucle vient de https://stackoverflow.com/a/10929511
        while IFS='' read -r ligne || [[ -n "$ligne" ]]; do
            # Récupération des éléments du rappel
            heureActuelle=$(echo $ligne | cut -d ':' -f 1)
            evenementPasse=$(echo $ligne | cut -d ':' -f 2)
            description=$(echo $ligne | cut -d ':' -f 3)
            tags=$(echo $ligne | cut -d ':' -f 4)
            let octalToDecimal=10#$heureActuelle
            if [[ $octalToDecimal -le $heure ]] && [[ $evenementPasse -eq 0 ]]; then
                sed -i "s/$ligne/$heureActuelle:1:$description:$tags/g" $FICHIER_TMP
                if [[ $octalToDecimal -eq $heure ]]; then
                    debug "Trouvé"
                    xmessage $description &
                fi
            fi
        done < "$FICHIER_HORAIRE"
        [[ -a $FICHIER_TMP ]] && cp $FICHIER_TMP $FICHIER_HORAIRE || echo "Erreur: le fichier $FICHIER_TMP n'existe pas !"
        sleep $BOUCLE_TEMPS_MISE_A_JOUR
    done
}

##
# Affiche les messages de debug si activés
##
function debug {
    [[ $DEBUG -eq 0 ]] && echo "[DEBUG] $1"
}

##
# Verifie si les fichiers nécessaires existent. Si l'un d'entre eux manque, il est créé.
# Aucun paramètres
##
function install {
    echo -n "Installation en cours... "
    test -d $DOSSIER_CONFIGURATION || mkdir -p $DOSSIER_CONFIGURATION
    test -a $FICHIER_BOUCLE || echo "" > $FICHIER_BOUCLE
    test -a $FICHIER_HORAIRE || touch $FICHIER_HORAIRE
    echo "Prêt."
}

##
# Supprime tous les fichiers de configuration pour nettoyer le système.
# Les fichiers exécutables ne sont pas impactés.
##
function uninstall {
    echo "Désinstallation en cours... "
    arreterBoucle
    rm $FICHIER_BOUCLE
    rm $FICHIER_HORAIRE
    rmdir $DOSSIER_CONFIGURATION
    echo "Désinstallation terminée."
}

##
# Permet de savoir si une boucle est en cours ou non.
# Retourne :
#   0 Une boucle est en cours
#   1 Aucune boucle n'est en cours
#   2 Le fichier de configuration de la boucle n'existe pas
##
function verifierBoucleExiste {
    local taillefichier=$(du -h --apparent-size $FICHIER_BOUCLE | cut -c1)
    debug "Boucle: Taille réelle fichier : $taillefichier"
    let taillefichier-- # Prise en compte du caractère \n en fin de fichier
    debug "Boucle: Taille fichier : $taillefichier"
    if [[ -a $FICHIER_BOUCLE ]]; then
        if [[ $taillefichier -eq 0 ]]; then
            return 1 # Fichier vide
        else
            return 0 # Boucle en cours
        fi
    else
        return 2 # Fichier inexistant
    fi
}

##
# Arrête la boucle en cours
##
function arreterBoucle {
    verifierBoucleExiste
    if [[ $? -eq 0 ]]; then
        kill $(cat $FICHIER_BOUCLE)
        echo "" > $FICHIER_BOUCLE  # On efface le PID
        echo "Boucle terminée, ce script a libéré toutes ses ressources."
    else
        echo "Aucune boucle n'est en cours."
    fi
}

##
# Lance une boucle si nécessaire.
##
function lancerBoucle {
    # Lance la boucle
    verifierBoucleExiste
    if [[ $? -eq 0 ]]
    then
        echo "Une boucle est déjà en cours."
        debug "Son PID est $(cat $FICHIER_BOUCLE)"
    elif [[ $? -eq 1 ]]
    then
        creerBoucle
    else
        echo "L'installation a été détectée incomplète, réparation."
        install
        creerBoucle
    fi
}

##
# Lance une boucle.
##
function creerBoucle {
    ./quitter.sh --loop &
    echo $! > $FICHIER_BOUCLE
    echo "Boucle lancée."
    debug "PID=$!"
}

##
# Affiche les rendez-vous
# $1 : 0 pour "seulement a venir" ou 1 pour tous
##
function afficherRendezVous {
    echo "~~ Recherche de rendez-vous ~~"
    [[ $2 = '' ]] || echo -n "Recherche : $2, "
    if [[ $1 -eq 0 ]]; then
        echo "Période : à venir"
        nbrRendezVous=$(wc -l $FICHIER_HORAIRE | cut -d ' ' -f 1)
        let nbrRendezVous++
        for (( i = 1; i < $nbrRendezVous; i++ )); do
            ligneActuelle=$(head -$i $FICHIER_HORAIRE | tail -1)
            status=$(echo $ligneActuelle | cut -d ':' -f 2) # 0 si a venir et 1 si deja passé
            test $status -eq 0 && afficher "$ligneActuelle" $2
        done
    else
        echo "Période : tout"
        while IFS='' read -r ligne || [[ -n "$ligne" ]]; do
            afficher "$ligne" $2
        done < "$FICHIER_HORAIRE"
    fi
}

##
# Formatte et affiche des données provenant de la base de données.
##
function afficher {
    if [[ $2 = '' ]]; then
        echo $1 | sed 's/:[01]:/ > /' | tr ':' ' '
    else
        echo $1 | grep $2 | sed 's/:[01]:/ > /' | tr ':' ' '
    fi
}

##
# Vérifie si une confirmation est nécessaire ou pas.
##
function supprimerRendezVous {
    case $1 in
    -f | --force)
        suppression $2;;
    -m | --manual)
        suppressionConfirmation $2;;
    *)
        suppressionConfirmation $1;;
    esac
}

##
# Suppresion d'un événement avec confirmation
##
function suppressionConfirmation {
    afficherRendezVous 1 $1
    echo ""
    # Confirmation inspirée de https://stackoverflow.com/a/27875395
    read -p "Êtes-vous sûr de vouloir supprimer ces événements ? (O/n) " reponse
    case ${reponse:0:1} in
    [oO]|[oO][uU][iI])
        suppression $1;;
    *)
        echo "Suppression annulée.";;
    esac
}

##
# Fonction technique de suppression (sans validation)
##
function suppression {
    sed -i "/$1/d" $FICHIER_HORAIRE
    echo "Rappels supprimés."
}

##
# Affiche l'aide.
##
function afficherAide {
    #Affiche l'aide
    echo "quitter.sh HHMM [Rappel] [+Tags] ..."
    echo "quitter.sh [Option]"
    echo "Permet de gérer des rappels."
    echo ""
    echo "      UTILISATIONS"
    echo ""
    echo "quitter HHMM [Rappel] [+TAGS]..."
    echo "  Ajoute un rappel a une heure donnée"
    echo ""
    echo "quitter [Option]"
    echo "  Arrête la boucle :"
    echo "      -q"
    echo "      --quit"
    echo ""
    echo "  Affiche la liste des rendez-vous à venir."
    echo "  On peut choisir de rechercher par tag, heure ou mot-clef :"
    echo "      -l [recherche]"
    echo "      --list-next [recherche]"
    echo ""
    echo "  Affiche la liste de tous les rendez-vous."
    echo "  On peut choisir de rechercher par tag, heure ou mot-clef :"
    echo "      -a [recherche]"
    echo "      --list-all [recherche]"
    echo ""
    echo "  Supprime des rendez-vous par tag, heure ou mot-clef :"
    echo "      -r       [-m | --manual] [recherche]"
    echo "      --delete [-m | --manual] [recherche]"
    echo "      --remove [-m | --manual] [recherche]"
    echo ""
    echo "  Supprime des rendez-vous par tag, heure ou mot-clef, sans confirmation :"
    echo "      -r        -f | --force   [recherche]"
    echo "      --delete  -f | --force   [recherche]"
    echo "      --remove  -f | --force   [recherche]"
    echo "      -rf                      [recherche]"
    echo ""
    echo "  Affiche l'aide (cette page) :"
    echo "      -h"
    echo "      --help"
    echo ""
    echo "  Crée les fichiers de configuration (créés dynamiquement par le script) :"
    echo "      -i"
    echo "      --install"
    echo ""
    echo "  Nettoie le PC (fichiers de configuration & boucle éventuelle) :"
    echo "      -u"
    echo "      --uninstall"
    echo ""
    echo "  Réinitialise le script (--uninstall puis --install) :"
    echo "      --reset"
    echo ""
    echo "      CREATION"
    echo ""
    echo "Auteurs :"
    echo "  Marius Pardo"
    echo "  Ivan Canet"
}

##
# Ajoute une ligne dans horaire.db
# $1 : l'heure du rendez vous
# $2.. $n : intitulé du rendez vous
# $n+1.. $k : tags
##
function ajouterRendezVous {
    # Vérifier que l'heure est valide
    octalToDecimal=$1
    if [[ $octalToDecimal =~ ^[2][0-3][0-5][0-9]$|^[0-1][0-9][0-5][0-9]$ ]]; then
        let heure=10#$(date +%H%M)
        let octalToDecimal=10#$octalToDecimal
        if [[ $octalToDecimal -lt $heure ]]; then
            echo "Cette heure est déjà passée !"
            return 1
        fi
        ligneAAjouter=$1
        ligneAAjouter=$ligneAAjouter # Pourquoi ?
        shift
        intitule=""
        tags=""
        for arg in "$@"
        do
            car1=$(echo $arg | cut -c1)
            if [[ $car1 = "+" ]]; then
                debug "Tag détecté : $arg"
                tags="$tags$arg "
            else
                debug "Argument détecté : $arg"
                intitule="$intitule$arg "
            fi
        done
        ligneAAjouter="$ligneAAjouter:0:$intitule:$tags"
        debug "Ajoût de la ligne : $ligneAAjouter"
        [[ -a $FICHIER_HORAIRE ]] || install
        echo $ligneAAjouter >> $FICHIER_HORAIRE
        echo "Rappel ajouté."
        debug "Contenu du fichier :"
        debug "$(cat $FICHIER_HORAIRE)"
        lancerBoucle
    else
        echo "L'heure précisée ($1) est invalide. Elle doit être donnée sous le format HHMM."
        echo "L'aide du script est disponible avec -h."
        exit 8
    fi
}

debug "Vous avez entré : $*"
case $1 in
-i | --install)
    install;;
-u | --uninstall)
    uninstall;;
--reset)
    uninstall;install;;
-q | --quit)
    arreterBoucle;;
-l | --list-next)
    afficherRendezVous 0 $2;;
-a | --list-all)
    afficherRendezVous 1 $2;;
-rf)
    shift;suppression $*;;
-r | --remove | --delete)
    shift;supprimerRendezVous $*;;
-h | --help)
    afficherAide;;
--loop) # Interne, pas d'appel par l'utilisateur
    lancerCommeBoucle;;
*)
    ajouterRendezVous $*;;
esac
